<?php
/*
Plugin Name: Invisible Defender Fork
Plugin URI:
Description: This plugin protects your comment form from spambots via reverse captcha (honeypot).
Author: Kailey Lampert
Version: 2013.01.31
Author URI: http://kaileylampert.com/
*/

if ( !class_exists( 'InvisibleDefender' ) || ( defined( 'WP_DEBUG') && WP_DEBUG ) ) {

	class InvisibleDefender {

		function __construct() {

			// Protect comments form
			add_action( 'comment_form', array( &$this, 'add_hidden_fields' ) );
			add_filter( 'preprocess_comment', array( &$this, 'preprocess_comment' ), 1 );

			add_action( 'activity_box_end', array( &$this, 'activity_box_end' ) );
		}

		// Add hidden fields to the form
		function add_hidden_fields() {
			echo '<div style="display:none">Please leave these two fields as-is: ';
			echo '<input type="text" name="blue41light" value="" />';
			echo '<input type="text" name="pretty49mug" value="1" />';
			echo '</div>';
		}

		// Protection function for submitted comment form
		function preprocess_comment( $commentdata ) {
			$this->check_hidden_fields();
			return $commentdata;
		}

		// Check for hidden fields and wp_die() in case of error
		function check_hidden_fields( $form_type = false ) {
			if ( false !== $form_type )
				_deprecated_argument( __FUNCTION__, '2013.01.31' );

			// Skip check for AJAX
			if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
				return;
			// Skip check for XML-RPC
			if ( defined( 'XMLRPC_REQUEST' ) && XMLRPC_REQUEST )
				return;
			// Skip check for Trackbacks/Pingbacks
			if ( is_trackback() )
				return;

			// Get values from POST data
			$val0 = $val1 = '';
			if ( isset( $_POST['blue41light'] ) ) {
				$val0 = $_POST['blue41light'];
			}
			if ( isset( $_POST['pretty49mug'] ) ) {
				$val1 = $_POST['pretty49mug'];
			}

			// Check values
			if ( ( $val0 != '' ) || ( $val1 != '1' ) ) {
				// Increase stats counter
				$stats = get_option( 'indef_stats' );
				++$stats[ 'comment' ];
				update_option( 'indef_stats', $stats );

				// if ( defined( 'INDEF_LOG_SPAMMERS' ) && INDEF_LOG_SPAMMERS ) {
				// 	$this->log_spammer( 'F' );
				// }

				// Block spammer!
				wp_die( 'This page was accessed in error.', '403 Forbidden', array( 'response' => 403 ) );
				exit;
			}
		}

		// Show stats in Dashboard
		function activity_box_end() {
			$stats = get_option( 'indef_stats' );
			$sum = $stats['register'] + $stats['login'] + $stats['comment'];
			if ( isset( $stats['blacklist'] ) ) { // Added in version 1.4.2
				$blacklist = $stats['blacklist'];
				$sum += $blacklist;
			} else {
				$blacklist = 0;
			}
			echo '<p>';
			echo sprintf( __('<strong>Invisible Defender</strong> showed <strong>403</strong> to <strong>%1$s</strong> bad guys (<strong>%2$s</strong> was blacklisted, <strong>%3$s</strong> on register form, <strong>%4$s</strong> on login form, <strong>%5$s</strong> on comments form).',
				'invisible-defender'), number_format_i18n( $sum ), number_format_i18n( $blacklist ),
				number_format_i18n( $stats['register'] ), number_format_i18n( $stats['login'] ),
				number_format_i18n ($stats['comment'] ) );
			echo '</p>';
		}

		// Log spam
		// function log_spammer( $code ) {
		// 	$ip = $_SERVER['REMOTE_ADDR'];
		// 	$host = gethostbyaddr( $ip );
		// 	$agent = isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '';
		// 	$timestamp = date( 'Y-m-d H:i:s' );
		// 	$line = "$code\t$timestamp\t$ip\t$host\t$agent\n";

		// 	$dir = dirname( __FILE__ ). '/logs/';
		// 	if ( !file_exists( $dir ) ) {
		// 		@mkdir( $dir );
		// 	}

		// 	$fname = $dir . date( 'Y-m-d' ) . '.txt';
		// 	$file = @fopen( $fname, 'a' );
		// 	if ( $file ) {
		// 		flock( $file,  LOCK_EX );
		// 		fwrite( $file, $line );
		// 		flock( $file,  LOCK_UN );
		// 		fclose( $file );
		// 	}
		// }

	}

	add_option( 'indef_stats', array( 'blacklist' => 0, 'register' => 0, 'login' => 0, 'comment' => 0 ) ); // Number of blocked bots

	$wp_invisible_defender = new InvisibleDefender();
} // END

//eof